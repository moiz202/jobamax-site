import Vue from 'vue'
import App from './App.vue'
import VueRouter from './router'
import './assets/css/style.css'

Vue.use(VueRouter)
Vue.config.productionTip = false

new Vue({
  //routes
  router:VueRouter,
  render: h => h(App)
}).$mount('#app')

