import Vue from 'vue'
import VueRouter  from 'vue-router'
import Home from '@/views/Home.vue'
import AboutUs from '@/views/AboutUs'
import JobList from '@/views/JobList'
import Security from '@/views/Security'
import ContactUs from '@/views/ContactUs'
import JobSeeker from '@/views/JobSeeker'
import JobRecuiter from '@/views/JobRecruiter'
import SubmitApplication from '@/views/SubmitApplication'
import CommunityGuideline from '@/views/CommunityGuideline'
import SafetyTips from '@/views/SafetyTips'
import Download from '@/views/Download'
import Subscription from '@/views/Subscription'
import Support from '@/views/Support'
import Terms from '@/views/Terms'
import Privacy from '@/views/Privacy'
import SubmitYourIdea from '@/views/SubmitYourIdea'
import Cv from '@/views/Cv'
Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history', // enable history mode
  routes: [
    {
      path: '/',
      component: Home,
      name: 'Home'
    },
    {
        path: '/about-us',
        component: AboutUs,
        name: 'AboutUs'
      },
      {
        path: '/job-list',
        component: JobList,
        name: 'JobList'
      },
      {
        path: '/security',
        component: Security,
        name: 'Security'
      },
      {
        path: '/contact-us',
        component: ContactUs,
        name: 'ContactUs'
      },
      {
        path: '/job-seeker',
        component: JobSeeker,
        name: 'JobSeeker'
      },
      {
        path: '/job-recruiter',
        component: JobRecuiter,
        name: 'JobRecuiter'
      },
      {
        path: '/submit-application',
        component: SubmitApplication,
        name: 'SubmitApplication'
      },
      {
        path: '/community-guideline',
        component: CommunityGuideline,
        name: 'CommunityGuidline'
      },
      {
        path: '/safety-tips',
        component:SafetyTips,
        name: 'SafetyTips'
      },
      {
        path: '/download',
        component:Download,
        name: 'Download'
      },
      {
        path: '/subscription',
        component: Subscription,
        name: 'Subscription' 
      },
      {
        path: '/support',
        component: Support,
        name: 'Support' 
      },
      {
        path: '/terms',
        component: Terms,
        name: 'Terms' 
      },
      {
        path: '/privacy',
        component: Privacy,
        name: 'Privacy' 
      },
      {
        path: '/submit-your-idea',
        component: SubmitYourIdea,
        name: 'SubmitYourIdea' 
      },
      {
        path: '/cv',
        component: Cv,
        name: 'Cv'
      }

  ],
  // linkActiveClass: "active",
  // linkExactActiveClass: "active"
})

export default router;
